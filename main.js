class Roulette extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: props.start,
      flag: false
    };
    this.start = Number(props.start);
    this.end = Number(props.end);
  }

  tick() {
    if (this.state.flag) {
      this.setState((state, props) => ({
        counter: state.counter % (1 + this.end - this.start) + this.start,
        flag: state.flag
      }));
    }
  }

  click = () => {
    this.setState((state, props) => ({
      counter: state.counter,
      flag: !state.flag
    }));
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.flag && !this.state.flag) {
      let rand = Math.floor(Math.random() * (Math.floor(this.end) - Math.ceil(this.start) + 1) + Math.ceil(this.start));
      console.log(rand);
      this.setState((state, props) => ({
        counter: rand,
        flag: state.flag
      }));

    }
  }

  componentDidMount() {      
      clearInterval(this.timerID);
      this.timerID = setInterval(
        () => this.tick(),
        50
      );
  }

  render() {
    return (
      <div onClick={this.click} id="roulette">
        <p>{this.state.counter}</p>
      </div>
    );
  }
}

(() =>  {
  let uri = new URL(location.href);
  
  ReactDOM.render(
    <Roulette start={uri.searchParams.get("start")} end={uri.searchParams.get("end")}/>,
    document.getElementById('root'));
}) ();
